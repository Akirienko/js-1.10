const inputPassword = document.getElementById("inputPassword");
const inputConfirm = document.getElementById("inputConfirm");
const btn = document.querySelector(".btn");
const faEye = document.querySelector(".fa-eye");
const faEyeSlash = document.querySelector(".fa-eye-slash");
const error = document.querySelector(".error");


faEye.addEventListener("click", () => {
  if (faEye.classList.contains("fa-eye")) {
    faEye.classList.remove("fa-eye");
    faEye.classList.add("fa-eye-slash");
    inputPassword.type = "password";

  } else {
    faEye.classList.remove("fa-eye-slash");
    faEye.classList.add("fa-eye");
    inputPassword.type = "text";
  }
});

faEyeSlash.addEventListener("click", () => {
  if (faEyeSlash.classList.contains("fa-eye-slash")) {
    faEyeSlash.classList.remove("fa-eye-slash");
    faEyeSlash.classList.add("fa-eye");
    inputConfirm.type = "text";

  } else {
    faEyeSlash.classList.remove("fa-eye");
    faEyeSlash.classList.add("fa-eye-slash");
    inputConfirm.type = "password";
  }
});

btn.addEventListener("click", (ev) => {
  if(inputPassword.value === inputConfirm.value) {
    alert("You are welcome");
  } else {
    error.removeAttribute("hidden");
  }
  ev.preventDefault();
})